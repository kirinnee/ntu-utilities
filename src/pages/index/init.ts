import {Core, Kore} from "@kirinnee/core";

let core: Core = new Kore();
core.ExtendPrimitives();

function CopyToClipBoard(s: string): void {
    const ta: HTMLTextAreaElement = document.getElementById("clipboard-sim")! as HTMLTextAreaElement;
    ta.value = s;
    ta.select();
    document.execCommand("copy");
}

export {
    core,
    CopyToClipBoard,
}
