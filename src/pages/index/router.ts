import Vue from 'vue'
import Router, {Route, RouteConfig} from 'vue-router'
import Home from "./components/Home.vue";
import IntegrationAssignment1 from "./views/IntegrationAssignment1.vue";
import IntegrationAssignment2 from "./views/IntegrationAssignment2.vue";


Vue.use(Router);

const routes: RouteConfig[] = [
	{path: '/', name: 'home', component: Home },
	{path: '/int1', name: 'int1', component: IntegrationAssignment1 },
	{path: '/int2', name: 'int2', component: IntegrationAssignment2},

];
const router: Router = new Router({
	mode: "history",
	base: process.env.BASE_URL,
	routes
});

//Middle Ware
router.beforeEach((to: Route, from: Route, next) => {
			return next();
});

export {router};
