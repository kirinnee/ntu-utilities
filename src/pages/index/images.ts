import {core} from "./init";
import i1q1 from "./assets/i1q1.png";
import i1q2 from "./assets/i1q2.png";
import i1q3 from "./assets/i1q3.png";
import i1q4 from "./assets/i1q4.png";
import i1q5 from "./assets/i1q5.png";
import i1q6 from "./assets/i1q6.png";
import i1q7 from "./assets/i1q7.png";
import i1q8 from "./assets/i1q8.png";
import i1q9 from "./assets/i1q9.png";
import i1q10 from "./assets/i1q10.png";

import i2q1 from "./assets/i2q1.png";
import i2q2 from "./assets/i2q2.png";
import i2q3 from "./assets/i2q3.png";
import i2q4 from "./assets/i2q4.png";
import i2q5 from "./assets/i2q5.png";
import i2q6 from "./assets/i2q6.png";
import i2q7 from "./assets/i2q7.png";
import i2q8 from "./assets/i2q8.png";
import i2q9 from "./assets/i2q9.png";
import i2q10 from "./assets/i2q10.png";

import i2a1 from "./assets/i2a1.jpg";
import i2a2 from "./assets/i2a2.jpg";
import i2a3 from "./assets/i2a3.jpg";
import i2a4 from "./assets/i2a4.jpg";
import i2a5 from "./assets/i2a5.jpg";
import i2a6 from "./assets/i2a6.jpg";
import i2a7 from "./assets/i2a7.jpg";
import i2a8 from "./assets/i2a8.jpg";
import i2a9 from "./assets/i2a9.jpg";
import i2a10 from "./assets/i2a10.jpg";

core.AssertExtend();

let images: any = {
    i1q1,
    i1q2,
    i1q3,
    i1q4,
    i1q5,
    i1q6,
    i1q7,
    i1q8,
    i1q9,
    i1q10,
    i2q1,
    i2q2,
    i2q3,
    i2q4,
    i2q5,
    i2q6,
    i2q7,
    i2q8,
    i2q9,
    i2q10,
    i2a1,
    i2a2,
    i2a3,
    i2a4,
    i2a5,
    i2a6,
    i2a7,
    i2a8,
    i2a9,
    i2a10,
};

export {
    images,
}
